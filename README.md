# README #

### USO MENU ###

* Tres botones implementados:
* Izquierda: para desplazarse a la izquierda en el menú principal y para volver atrás en los submenús
* Derecha: para desplazarse a la dereche en el menú principal
* Enter: para entrar al submenú y encender/apagar cosas

### CONFIGURACIÓN DE PINES ###

* PA0  : botón derecho
* PA1  : botón centro
* PA2  : botón izquierdo
* PA3  : sensor nivel de agua
* PA5  : enable PWM motor B puente H
* PE9  : echo pin sensor ultrasonido
* PE10 : trigger pin sensor ultrasonido
* PE11 : enable PWM motor A puente H
* PB7  : SDA comunicación I2C
* PB6  : SCL comunicación I2C
* PA10 : IN1 motor paso a paso
* PA9  : IN2 motor paso a paso
* PA8  : IN3 motor paso a paso
* PC9  : IN4 motor paso a paso
* PD8  : IN4 puente H
* PD9  : IN3 puente H
* PD10 : IN2 puente H
* PD11 : IN1 puente H
