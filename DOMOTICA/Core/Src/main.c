/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//#include "lcd_i2cModule.h"
#include "i2c-lcd.h"
#include "stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim6;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM6_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//Variables menú
typedef enum {INICIO, PUERTA, MOVIENDO_PUERTA, VENTILACION, TEMPERATURA_MOTOR, PERSIANA, MOVIENDO_PERSIANA, AGUA, NIVEL_AGUA} modosMenu;

modosMenu estados_pantalla = INICIO;
volatile int left = 0; //botón izquierdo
volatile int right = 0; //botón derecho
volatile int enter = 0; //botón centro

//Variables presencia
volatile int puerta_encendida=0; //controla el encendido de la puerta
volatile int presencia=0; //flag para detectar presencia cerca de la puerta

//Variables sensor ultrasonido
uint32_t IC_Val1 = 0;
uint32_t IC_Val2 = 0;
uint32_t Difference = 0;
uint8_t Is_First_Captured = 0;  // is the first value captured ?
uint8_t Distance  = 0;
uint8_t ValorDistancia=0;

//Variables sensores análogicos (temperatura y agua)
uint16_t nivel_agua;
uint16_t temperatura;
uint16_t adc_value[2];
uint16_t adc_buffer[2];
volatile int validar = 0; //1-permite actualizar medición de la temperatura
volatile int motor_ventilador = 0; //0-apagado, 1-velocidad lento 2-velocidad normal 3-velocidad rapido

//Variables paso a paso
volatile int abriendo_puerta; //0-cerrada 1-abierta 2-abriendo 3-cerrando
volatile int mensajes; //0 desactivados, 1 activados
int paso[8][4]={
			{1,0,0,0},
			{1,1,0,0},
			{0,1,0,0},
			{0,1,1,0},
			{0,0,1,0},
			{0,0,1,1},
			{0,0,0,1},
			{1,0,0,1},
	};

//Variables motor persiana
volatile int posicion_persiana = 0; //0-bajada 1-subida
volatile int motor_persiana = 0; //0-persiana quieta 1-moviendo persiana


//******************************************GENERAL*************************************
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	if(hadc->Instance == ADC1){
		adc_value[0] = adc_buffer[0];
		adc_value[1] = adc_buffer[1];
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){

	if (GPIO_Pin == RIGHT_BUTTON_Pin){
		right = 1;
		left = 0;
		enter = 0;
	}
	else if (GPIO_Pin == ENTER_BUTTON_Pin){
		right = 0;
		left = 0;
		enter = 1;
	}
	else if (GPIO_Pin == LEFT_BUTTON_Pin){
		right = 0;
		left = 1;
		enter = 0;
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim){
	if (htim->Instance == TIM6){
			validar = 1;
	}
}

//*******************************************SENSOR ULTRASONIDO*******************************
void delay (uint16_t time)
{
	__HAL_TIM_SET_COUNTER(&htim1, 0);
	while (__HAL_TIM_GET_COUNTER (&htim1) < time);
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)  // if the interrupt source is channel1
	{
		if (Is_First_Captured==0) // if the first value is not captured
		{
			IC_Val1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1); // read the first value
			Is_First_Captured = 1;  // set the first captured as true
			// Now change the polarity to falling edge
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_1, TIM_INPUTCHANNELPOLARITY_FALLING);
		}

		else if (Is_First_Captured==1)   // if the first is already captured
		{
			IC_Val2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);  // read second value
			__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter

			if (IC_Val2 > IC_Val1)
			{
				Difference = IC_Val2-IC_Val1;
			}

			else if (IC_Val1 > IC_Val2)
			{
				Difference = (0xffff - IC_Val1) + IC_Val2;
			}

			Distance = Difference * .034/2;
			Is_First_Captured = 0; // set it back to false

			// set polarity to rising edge
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_1, TIM_INPUTCHANNELPOLARITY_RISING);
			__HAL_TIM_DISABLE_IT(&htim1, TIM_IT_CC1);

		}

	}
	if (Distance < 6 && puerta_encendida){ //señal de alarma
		presencia=1;
	}
	else{
		presencia=0;
	}
}

void HCSR04_Read (void)
{
	HAL_GPIO_WritePin(TRIGGER_PIN_GPIO_Port, TRIGGER_PIN_Pin, GPIO_PIN_SET);  // pull the TRIG pin HIGH
	delay(10);  // wait for 10 us
	HAL_GPIO_WritePin(TRIGGER_PIN_GPIO_Port, TRIGGER_PIN_Pin, GPIO_PIN_RESET);  // pull the TRIG pin low

	__HAL_TIM_ENABLE_IT(&htim1, TIM_IT_CC1);
}

//*******************************************MOTOR PASO A PASO******************************
void pasoApaso(void){

	HAL_GPIO_WritePin(LED_AZUL_GPIO_Port, LED_AZUL_Pin, GPIO_PIN_SET);
	//Abrimos puerta
		if(mensajes==1){
			lcd_clear();
			lcd_put_cur(0,0);
			lcd_send_string("Puerta ");
			lcd_put_cur(1,0);
			lcd_send_string("Abriendose");
		}
		for(int i=0; i<512;i++){
			for(int j=0; j<8;j++){
				HAL_GPIO_WritePin(IN1_GPIO_Port, IN1_Pin, paso[j][0]);
				HAL_GPIO_WritePin(IN2_GPIO_Port, IN2_Pin, paso[j][1]);
				HAL_GPIO_WritePin(IN3_GPIO_Port, IN3_Pin, paso[j][2]);
				HAL_GPIO_WritePin(IN4_GPIO_Port, IN4_Pin, paso[j][3]);
				HAL_Delay(10);
			}
		}

		//Puerta abierta
		if(mensajes==1){
			lcd_clear();
			lcd_put_cur(0,0);
			lcd_send_string("Puerta ");
			lcd_put_cur(1,0);
			lcd_send_string("Abierta");
		}
		HAL_Delay(3000);

		//Cerramos puerta
		if(mensajes==1){
			lcd_clear();
			lcd_put_cur(0,0);
			lcd_send_string("Puerta ");
			lcd_put_cur(1,0);
			lcd_send_string("Cerrandose");
		}

		for(int i=0; i<512;i++){
			for(int j=0; j<8;j++){
				HAL_GPIO_WritePin(IN1_GPIO_Port, IN1_Pin, paso[j][3]);
				HAL_GPIO_WritePin(IN2_GPIO_Port, IN2_Pin, paso[j][2]);
				HAL_GPIO_WritePin(IN3_GPIO_Port, IN3_Pin, paso[j][1]);
				HAL_GPIO_WritePin(IN4_GPIO_Port, IN4_Pin, paso[j][0]);
				HAL_Delay(10);
			}
		}

		presencia = 0;

		//Puerta cerrada
		if(mensajes==1){
			lcd_clear();
			lcd_put_cur(0,0);
			lcd_send_string("Puerta ");
			lcd_put_cur(1,0);
			lcd_send_string("Cerrada");
		}
		HAL_GPIO_WritePin(LED_AZUL_GPIO_Port, LED_AZUL_Pin, GPIO_PIN_RESET);
}

void mensajesPasoAPaso(void){

	mensajes = 1;

	if(puerta_encendida && presencia == 0){
			lcd_clear();
			lcd_put_cur(0,0);
			lcd_send_string("Puerta");
			lcd_put_cur(1,0);
			lcd_send_string("Cerrada");
			HAL_GPIO_WritePin(LED_VERDE_GPIO_Port, LED_VERDE_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(LED_ROJO_GPIO_Port, LED_ROJO_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED_AZUL_GPIO_Port, LED_AZUL_Pin, GPIO_PIN_RESET);
		}
		else if (puerta_encendida && presencia){ // se activa la emergencia
			pasoApaso();
		}
		else {
			lcd_clear();
			lcd_put_cur(0,0);
			lcd_send_string("Puerta");
			lcd_put_cur(1,0);
			lcd_send_string("Apagada");
			HAL_GPIO_WritePin(LED_VERDE_GPIO_Port, LED_VERDE_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED_AZUL_GPIO_Port, LED_AZUL_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED_ROJO_GPIO_Port, LED_ROJO_Pin, GPIO_PIN_SET);
		}
}

//**************************************SENSOR TEMPERATURA*********************************
uint16_t calculoTemp(void){

	if (validar){
		temperatura = ((((float)adc_buffer[0]) - 0.76)/0.0025) + 25;
		temperatura/=1000;
		validar = 0;
		return temperatura;
	}
	return 0;
}

void ventiladorLento(){

	if(temperatura >= 25){
		motor_ventilador = 1;
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 650);
		HAL_GPIO_WritePin(GPIOD, IN3_PUENTE_H_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOD, IN4_PUENTE_H_Pin,GPIO_PIN_RESET);
	}
}

void ventiladorNormal(){

	if(temperatura >= 25){
		motor_ventilador = 2;
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 1200);
		HAL_GPIO_WritePin(GPIOD, IN3_PUENTE_H_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOD, IN4_PUENTE_H_Pin,GPIO_PIN_RESET);
	}
}

void ventiladorRapido(){

	if(temperatura >= 25){
		motor_ventilador = 3;
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 3000);
		HAL_GPIO_WritePin(GPIOD, IN3_PUENTE_H_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOD, IN4_PUENTE_H_Pin,GPIO_PIN_RESET);
	}
}

void ventiladorParado(){

	motor_ventilador = 0;
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 0);
	HAL_GPIO_WritePin(GPIOD, IN3_PUENTE_H_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, IN4_PUENTE_H_Pin,GPIO_PIN_RESET);
}

void mensajesTemperatura(){

	char str[20]  = {0};
	calculoTemp();
	lcd_clear();
	lcd_put_cur(0,0);
	sprintf(str, "TEMP: %d", temperatura);
	lcd_send_string(str);
	lcd_send_data('C');

	if(motor_ventilador != 0){
		lcd_put_cur(1,0);
		lcd_send_string("MOTOR ON");
	}
	else if (motor_ventilador == 0){
		lcd_put_cur(1,0);
		lcd_send_string("MOTOR OFF");
	}

}

//****************************************SENSOR NIVEL DE AGUA********************************
uint16_t calculoNivel(){
	nivel_agua = (0.035454*(float)adc_buffer[1])-20.2727;
	return nivel_agua;
}

void mensajesNivelAgua(){

	char str[20]  = {0};
	calculoNivel();

	if(nivel_agua < 10){
		lcd_clear();
		lcd_put_cur(0,0);
		lcd_send_string("ALERTA");
		lcd_put_cur(1, 0);
		lcd_send_string("Agua < 10 mm");
	}
	else{
		lcd_clear();
		lcd_put_cur(0,0);
		sprintf(str, "Agua : %d mm", nivel_agua);
		lcd_send_string(str);
	}

}

//**************************************MOTOR CC PERSIANA*************************************
void subirPersiana(int pulse)
{
	motor_persiana = 1;
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, pulse);
	HAL_GPIO_WritePin(GPIOD, IN2_PUENTE_H_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, IN1_PUENTE_H_Pin,GPIO_PIN_SET);
}

void bajarPersiana(int pulse)
{
	motor_persiana = 1;
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, pulse);
	HAL_GPIO_WritePin(GPIOD, IN2_PUENTE_H_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD, IN1_PUENTE_H_Pin,GPIO_PIN_RESET);
}

void pararPersiana()
{
	motor_persiana = 0;
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, 0);
	HAL_GPIO_WritePin(GPIOD, IN2_PUENTE_H_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, IN1_PUENTE_H_Pin,GPIO_PIN_RESET);
}

void mensajesPersiana(){
	if(posicion_persiana == 0 && motor_persiana == 0){
		lcd_clear();
		lcd_put_cur(0,0);
		lcd_send_string("Persiana");
		lcd_put_cur(1,0);
		lcd_send_string("Bajada");
	}

	else if(posicion_persiana == 1 && motor_persiana == 0){
		lcd_clear();
		lcd_put_cur(0,0);
		lcd_send_string("Persiana");
		lcd_put_cur(1,0);
		lcd_send_string("Subida");
	}
}

//**************************************MENÚ PANTALLA****************************************
int debouncer (volatile int *button, GPIO_TypeDef* GPIO_port, uint16_t GPIO_number){
	static uint8_t button_count = 0;
	static int counter = 0;

	if(*button == 1){
		if(button_count == 0){
			counter = HAL_GetTick();
			button_count++;
		}
		if(HAL_GetTick() - counter >= 20){
			counter = HAL_GetTick();
			if(HAL_GPIO_ReadPin(GPIO_port, GPIO_number) != 1)
				button_count = 1;
			else
				button_count++;
			if(button_count == 4){
				button_count = 0;
				*button = 0;
				return 1;
			}
		}
	}
	return 0;
}

void printModos(){

	switch(estados_pantalla){
		case INICIO:
			lcd_clear();
			lcd_put_cur(0, 0);
			lcd_send_string("BIENVENIDO");
			break;

		case PUERTA:
			lcd_clear();
			lcd_put_cur(0, 0);
			lcd_send_string("PUERTA");
			break;

		case MOVIENDO_PUERTA:
			mensajesPasoAPaso();
			break;

		case VENTILACION:
			lcd_clear();
			lcd_put_cur(0, 0);
			lcd_send_string("VENTILACION");
			break;

		case TEMPERATURA_MOTOR:
			mensajesTemperatura();
			break;

		case PERSIANA:
			lcd_clear();
			lcd_put_cur(0, 0);
			lcd_send_string("PERSIANA");
			break;

		case MOVIENDO_PERSIANA:
			mensajesPersiana();
			break;

		case AGUA:
			lcd_clear();
			lcd_put_cur(0, 0);
			lcd_send_string("DEPOSITO AGUA");
			break;

		case NIVEL_AGUA:
			mensajesNivelAgua();
			break;
	}
}

void transicionModos(){

	switch (estados_pantalla){
		case INICIO:
			if(debouncer(&enter, ENTER_BUTTON_GPIO_Port, ENTER_BUTTON_Pin)){
				estados_pantalla = PUERTA;
				enter = 0;
			}
			break;

		case PUERTA:
			if (debouncer(&enter, ENTER_BUTTON_GPIO_Port, ENTER_BUTTON_Pin)){
				estados_pantalla = MOVIENDO_PUERTA;
				enter = 0;
			}
			else if (debouncer(&left, LEFT_BUTTON_GPIO_Port, LEFT_BUTTON_Pin)){
				estados_pantalla = VENTILACION;
				left = 0;
			}
			else if (debouncer(&right, RIGHT_BUTTON_GPIO_Port, RIGHT_BUTTON_Pin)){
				estados_pantalla = PERSIANA;
				right = 0;
			}
			break;

		case MOVIENDO_PUERTA:
			if(debouncer(&enter, ENTER_BUTTON_GPIO_Port, ENTER_BUTTON_Pin)){
				if(!puerta_encendida){
					puerta_encendida = 1;
					enter = 0;
				}
				else if (puerta_encendida){
					puerta_encendida = 0;
					enter = 0;
				}
			}
			if(debouncer(&left, LEFT_BUTTON_GPIO_Port, LEFT_BUTTON_Pin)){
				estados_pantalla = PUERTA;
				left = 0;
				//mensajes = 0;
			}
			break;

		case VENTILACION:
			if(debouncer(&enter, ENTER_BUTTON_GPIO_Port, ENTER_BUTTON_Pin)){
				estados_pantalla = TEMPERATURA_MOTOR;
				enter = 0;
			}
			else if (debouncer(&left, LEFT_BUTTON_GPIO_Port, LEFT_BUTTON_Pin)){
				estados_pantalla = AGUA;
				left = 0;
			}
			else if(debouncer(&right, RIGHT_BUTTON_GPIO_Port, RIGHT_BUTTON_Pin)){
				estados_pantalla = PUERTA;
				right = 0;
			}
			break;

		case TEMPERATURA_MOTOR:
			if(debouncer(&enter, ENTER_BUTTON_GPIO_Port, ENTER_BUTTON_Pin)){
				if(motor_ventilador == 0){
					motor_ventilador = 1;
					ventiladorLento();
					enter = 0;
				}

				else if(motor_ventilador != 0){
					motor_ventilador = 0;
					ventiladorParado();
					enter = 0;
				}
			}
			else if(debouncer(&left, LEFT_BUTTON_GPIO_Port, LEFT_BUTTON_Pin)){
				estados_pantalla = VENTILACION;
				left = 0;
			}
			else if(debouncer(&right, RIGHT_BUTTON_GPIO_Port, RIGHT_BUTTON_Pin)){
				if(motor_ventilador == 1){
					ventiladorNormal();
					right = 0;
				}
				else if(motor_ventilador == 2){
					ventiladorRapido();
					right = 0;
				}
				else if(motor_ventilador == 3){
					ventiladorLento();
					right = 0;
				}
			}
			break;

		case PERSIANA:
			if (debouncer(&enter, ENTER_BUTTON_GPIO_Port, ENTER_BUTTON_Pin)){
				estados_pantalla = MOVIENDO_PERSIANA;
				enter = 0;
			}
			else if (debouncer(&left, LEFT_BUTTON_GPIO_Port, LEFT_BUTTON_Pin)){
				estados_pantalla = PUERTA;
				left = 0;
			}
			else if (debouncer(&right, RIGHT_BUTTON_GPIO_Port, RIGHT_BUTTON_Pin)){
				estados_pantalla = AGUA;
				right = 0;
			}
			break;

		case MOVIENDO_PERSIANA:
			if (debouncer(&enter, ENTER_BUTTON_GPIO_Port, ENTER_BUTTON_Pin)){
				if(motor_persiana == 0 && posicion_persiana == 0){
					subirPersiana(2000);
					HAL_Delay(2500);
					pararPersiana();
					posicion_persiana = 1;
					enter = 0;
				}
				else if (motor_persiana == 0 && posicion_persiana == 1){
					bajarPersiana(2000);
					HAL_Delay(2500);
					pararPersiana();
					posicion_persiana = 0;
					enter = 0;
				}
			}
			else if(debouncer(&left, LEFT_BUTTON_GPIO_Port, LEFT_BUTTON_Pin)){
				estados_pantalla = PERSIANA;
				left = 0;
			}
			break;

		case AGUA:
			if (debouncer(&enter, ENTER_BUTTON_GPIO_Port, ENTER_BUTTON_Pin)){
				estados_pantalla = NIVEL_AGUA;
				enter = 0;
			}
			else if (debouncer(&left, LEFT_BUTTON_GPIO_Port, LEFT_BUTTON_Pin)){
				estados_pantalla = PERSIANA;
				left = 0;
			}
			else if (debouncer(&right, RIGHT_BUTTON_GPIO_Port, RIGHT_BUTTON_Pin)){
				estados_pantalla = VENTILACION;
				right = 0;
			}
			break;

		case NIVEL_AGUA:
			if (debouncer(&left, LEFT_BUTTON_GPIO_Port, LEFT_BUTTON_Pin)){
				estados_pantalla = AGUA;
				enter = 0;
			}
			break;
	}

}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  MX_TIM6_Init();
  /* USER CODE BEGIN 2 */
  //LCD
  lcd_init();

  //Ultrasonidos
  HAL_TIM_IC_Start_IT(&htim1,TIM_CHANNEL_1); //empezamos en el modo de interrupcion
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1); //control persianas
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2); //control ventilador
  //Sensor temperatura
  HAL_TIM_Base_Start_IT(&htim6);
  HAL_ADC_Start_DMA(&hadc1, adc_buffer, 2);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  if (presencia){
		  pasoApaso();
	  }
	  HCSR04_Read();//LECTURA DEL ULTRASONIDOS

	  HAL_Delay(250);

	  printModos();
	  transicionModos();

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 84;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 72-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 4000-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_IC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 84-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 2000-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 8400-1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 50400-1;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(TRIGGER_PIN_GPIO_Port, TRIGGER_PIN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, IN4_PUENTE_H_Pin|IN3_PUENTE_H_Pin|IN2_PUENTE_H_Pin|IN1_PUENTE_H_Pin
                          |LED_VERDE_Pin|LED_ROJO_Pin|LED_AZUL_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(IN4_GPIO_Port, IN4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, IN3_Pin|IN2_Pin|IN1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : RIGHT_BUTTON_Pin ENTER_BUTTON_Pin LEFT_BUTTON_Pin */
  GPIO_InitStruct.Pin = RIGHT_BUTTON_Pin|ENTER_BUTTON_Pin|LEFT_BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : TRIGGER_PIN_Pin */
  GPIO_InitStruct.Pin = TRIGGER_PIN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(TRIGGER_PIN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : IN4_PUENTE_H_Pin IN3_PUENTE_H_Pin IN2_PUENTE_H_Pin IN1_PUENTE_H_Pin
                           LED_VERDE_Pin LED_ROJO_Pin LED_AZUL_Pin */
  GPIO_InitStruct.Pin = IN4_PUENTE_H_Pin|IN3_PUENTE_H_Pin|IN2_PUENTE_H_Pin|IN1_PUENTE_H_Pin
                          |LED_VERDE_Pin|LED_ROJO_Pin|LED_AZUL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : IN4_Pin */
  GPIO_InitStruct.Pin = IN4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(IN4_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : IN3_Pin IN2_Pin IN1_Pin */
  GPIO_InitStruct.Pin = IN3_Pin|IN2_Pin|IN1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
