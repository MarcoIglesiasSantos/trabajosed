/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RIGHT_BUTTON_Pin GPIO_PIN_0
#define RIGHT_BUTTON_GPIO_Port GPIOA
#define RIGHT_BUTTON_EXTI_IRQn EXTI0_IRQn
#define ENTER_BUTTON_Pin GPIO_PIN_1
#define ENTER_BUTTON_GPIO_Port GPIOA
#define ENTER_BUTTON_EXTI_IRQn EXTI1_IRQn
#define LEFT_BUTTON_Pin GPIO_PIN_2
#define LEFT_BUTTON_GPIO_Port GPIOA
#define LEFT_BUTTON_EXTI_IRQn EXTI2_IRQn
#define NIVEL_AGUA_Pin GPIO_PIN_3
#define NIVEL_AGUA_GPIO_Port GPIOA
#define ENA_PUENTE_H_Pin GPIO_PIN_5
#define ENA_PUENTE_H_GPIO_Port GPIOA
#define ECHO_PIN_Pin GPIO_PIN_9
#define ECHO_PIN_GPIO_Port GPIOE
#define TRIGGER_PIN_Pin GPIO_PIN_10
#define TRIGGER_PIN_GPIO_Port GPIOE
#define IN4_PUENTE_H_Pin GPIO_PIN_8
#define IN4_PUENTE_H_GPIO_Port GPIOD
#define IN3_PUENTE_H_Pin GPIO_PIN_9
#define IN3_PUENTE_H_GPIO_Port GPIOD
#define IN2_PUENTE_H_Pin GPIO_PIN_10
#define IN2_PUENTE_H_GPIO_Port GPIOD
#define IN1_PUENTE_H_Pin GPIO_PIN_11
#define IN1_PUENTE_H_GPIO_Port GPIOD
#define LED_VERDE_Pin GPIO_PIN_12
#define LED_VERDE_GPIO_Port GPIOD
#define LED_ROJO_Pin GPIO_PIN_14
#define LED_ROJO_GPIO_Port GPIOD
#define LED_AZUL_Pin GPIO_PIN_15
#define LED_AZUL_GPIO_Port GPIOD
#define IN4_Pin GPIO_PIN_9
#define IN4_GPIO_Port GPIOC
#define IN3_Pin GPIO_PIN_8
#define IN3_GPIO_Port GPIOA
#define IN2_Pin GPIO_PIN_9
#define IN2_GPIO_Port GPIOA
#define IN1_Pin GPIO_PIN_10
#define IN1_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
