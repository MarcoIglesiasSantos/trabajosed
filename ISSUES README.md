# ISSUES README #

### Problemas a resolver (14/01/2021) ###

* Dentro del menú de la puerta, solo se apaga si salimos. Podríamos implementar que poder apagarla dentro pero es un poco movida
* El sensor de temperatura no actualiza constantemente la temperatura ¿Por usar IT? Idk

### Problemas a resolver (03/02/2021) ###

* El control de persiana con puente H solo mueve el motor hacia un sentido 
* Actualización: ya gira jeje

### Problemas a resolver (04/02/2021) ###

* Sensor ultrasonido

### Posibles mejoras (04/02/2021) ###

* Evitar que el sensor de temperatura actualice constantemente el valor por pantalla implementando un temporizador básico

